using static System.Console;

namespace CoreEscuela.Util
{
    public static class Printer
    {
        public static void DrawLinea(int tam = 10)
        {
            WriteLine("".PadLeft(tam, '='));
        }

	public static void PresioneENTER()
	{
	    WriteLine("Presione ENTER para continuar");
	}

        public static void WriteTitle(string titulo)
        {
            var tamano = titulo.Length + 4;
            DrawLinea(tamano);
            WriteLine($"| {titulo} |");
            DrawLinea(tamano);
        }

    }
}
