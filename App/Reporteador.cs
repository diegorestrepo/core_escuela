using System;
using System.Linq;
using System.Collections.Generic;
using CoreEscuela.Entidades;

namespace CoreEscuela.App
{
    public class Reporteador
    {
	
	Dictionary<LlaveDiccionario, IEnumerable<ObjetoEscuelaBase>> _diccionario;

	public Reporteador(Dictionary<LlaveDiccionario, IEnumerable<ObjetoEscuelaBase>> dicObsEsc)
	{
	    if (dicObsEsc == null)
	    {
		throw new ArgumentNullException(nameof(dicObsEsc));
	    }

	    _diccionario = dicObsEsc;
	}

	public IEnumerable<Evaluacion> GetListaEvaluaciones()
	{
	    if (_diccionario.TryGetValue(LlaveDiccionario.Evaluacion, out IEnumerable<ObjetoEscuelaBase> lista))
	    {
		return lista.Cast<Evaluacion>();
	    }
	    else
	    {
		return new List<Evaluacion>();
	    }
	}

	public IEnumerable<string> GetListaAsignaturas()
	{   
	    return GetListaAsignaturas(out var dummy);
	}

	public IEnumerable<string> GetListaAsignaturas(out IEnumerable<Evaluacion> listaEvaluaciones)
	{
	    listaEvaluaciones = GetListaEvaluaciones();
	    
	    return (from ev in listaEvaluaciones select ev.Asignatura.Nombre).Distinct();
	}

	public Dictionary<string, IEnumerable<Evaluacion>> GetDicEvaluacionesXAsignatura()
	{
	    var dictRta = new Dictionary<string, IEnumerable<Evaluacion>>();
	    var listaAsig = GetListaAsignaturas(out var listaEval);

	    foreach (var asig in listaAsig)
	    {
		var evalsAsig = from eval in listaEval where eval.Asignatura.Nombre == asig select eval;
		dictRta.Add(asig, evalsAsig);
	    }

	    return dictRta;
	}

	public Dictionary<string, IEnumerable<Object>> GetPromedioAlunmPorAsingatura()
	{
	    var rta = new Dictionary<string, IEnumerable<Object>>();
	    var dicEvalXAsig = GetDicEvaluacionesXAsignatura();

	    foreach (var asigConEval in dicEvalXAsig)
	    {
		var promediosAlumn = from eval in asigConEval.Value
			    group eval by new { eval.Alumno.UniqueId, eval.Alumno.Nombre }
			    into grupoEvalsAlumno
		            select new AlumnoPromedio 
			    { 
				alumnoid = grupoEvalsAlumno.Key.UniqueId,
				alumnoNombre = grupoEvalsAlumno.Key.Nombre,
				promedio = grupoEvalsAlumno.Average(evaluacion => evaluacion.Nota)
			    };

		rta.Add(asigConEval.Key, promediosAlumn);
	    } 



	    return rta;
	}
    }
}
