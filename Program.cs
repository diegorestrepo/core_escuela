﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoreEscuela.Entidades;
using CoreEscuela.App;
using CoreEscuela.Util;
using static System.Console;

namespace CoreEscuela
{
    class Program
    {
        static void Main(string[] args)
        {
	    AppDomain.CurrentDomain.ProcessExit += AccionDelEvento;

	    var engine = new EscuelaEngine();
            engine.Inicializar();
            Printer.WriteTitle("BIENVENIDOS A LA ESCUELA");
            
	    var reporteador = new Reporteador(engine.GetDiccionarioObjetos());
	    var evallist = reporteador.GetListaEvaluaciones();
	    var listaAsg = reporteador.GetListaAsignaturas();
	    var listaEvalXAsig = reporteador.GetDicEvaluacionesXAsignatura();
	    var listaPromXAsig = reporteador.GetPromedioAlunmPorAsingatura();

	    Printer.WriteTitle("Captura de una Evaluación por Consola");
	    var newEval = new Evaluacion();
	    string nombre, notaString;
	    float nota;

	    WriteLine("Ingrese el nombre de la evalución");
	    Printer.PresioneENTER();
	    nombre = ReadLine();

	    if (string.IsNullOrWhiteSpace(nombre))
	    {
		Printer.WriteTitle("El valor del nombre no puede ser vacío");
		WriteLine("Saliendo del programa");
	    }
	    else
	    {
		newEval.Nombre = nombre.ToLower();
		WriteLine("El nombre de la evalución a sido ingresado correctamente");
	    }

	    WriteLine("Ingrese la nota de la evalución");
	    Printer.PresioneENTER();
	    notaString = ReadLine();

	    if (string.IsNullOrWhiteSpace(notaString))
	    {
		Printer.WriteTitle("El valor la nota no puede ser vacío");
		WriteLine("Saliendo del programa");
	    }
	    else
	    {
		try
		{
		    newEval.Nota = float.Parse(notaString);

		    if (newEval.Nota < 0 || newEval.Nota > 5)
		    {
			throw new ArgumentOutOfRangeException("La nota debe estar entre 0 y 5");
		    }

		    WriteLine("La nota de la evalución a sido ingresado correctamente");
		}
		catch(ArgumentOutOfRangeException arge)
		{
		    Printer.WriteTitle(arge.Message);
		    WriteLine("Saliendo del programa");
		}
		catch(Exception)
		{
		    Printer.WriteTitle("El valor la nota no es un número");
		    WriteLine("Saliendo del programa");
		}
		finally
		{
		    Printer.WriteTitle("FINALY");
		}
	    }
        }

	private static void AccionDelEvento(object sender, EventArgs e)
	{
	    Printer.WriteTitle("SALIENDO");
	}

        private static void ImprimirCursosEscuela(Escuela escuela)
        {
            Printer.WriteTitle("Cursos de la escuela");

            if (escuela?.Cursos != null)
            {
                foreach (var curso in escuela.Cursos)
                {
                    WriteLine($"Nombre {curso.Nombre}, Id {curso.UniqueId}");
                }
            }
        }
    }
}
